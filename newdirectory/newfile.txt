public void ThenTheUserVerifiesThe_FilterByNameOnThePromptingFilterWindowIs_(string filterValueField, string filterName)

		{

			string filterByName = Common.WaitForElementVisible(By.XPath("//tr[contains(@data-filter-name, '"  filterValueField  "')]/descendant::td")).Text;

			Assert.AreEqual(filterName, filterByName);

		}